import Server from "lume/core/server.ts";
import expires from "lume/middlewares/expires.ts"

const server = new Server({
	port: 8000,
	root: `${Deno.cwd()}/_site`,
});

// Set Access-Control-Allow-Origin header to allow all origins
server.use(async (request, next) => {
	// Here you can modify the request before being passed to next middlewares
	const response = await next(request);

	response.headers.set('Access-Control-Allow-Origin', '*')
	// Here you can modify the response before being returned to the previous middleware
	return response;
});


server.use(expires())

server.start();

console.log("Listening on http://localhost:8000");